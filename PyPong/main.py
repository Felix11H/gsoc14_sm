#! /usr/bin/env python

import getopt
import sys

def print_usage ():
    """Print usage information for mypong"""

    print 'Usage information for mypong:'
    print '  mypong [--player1=name [--player2=name]]'
    print '  mypong --help   print this usage information and exit'



if __name__ == '__main__':
    longopts = ['help', 'player1=', 'player2=']

    try:
        opts, args = getopt.getopt(sys.argv[1:], '', longopts)
    except getopt.GetoptError, err:
        print 'Error:', str(err)
        print_usage()
        sys.exit(2)

    player1 = 'Player 1'
    player2 = 'Player 2'

    for o, a in opts:
        if o ==  '--help':
            print_usage()
            sys.exit()
        if o == '--player1':
            player1 = a
        if o == '--player2':
            player2 = a

    from game.engine import Engine
    Game = Engine(player1, player2)
    Game.run()
