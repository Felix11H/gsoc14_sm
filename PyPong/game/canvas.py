
import pygame
from pygame.locals import *    


class Canvas(object):

    """ sets the table of the game and lets engine draw the background """


    def __init__(self):   
        
        #set the pygame display
        self.screen = pygame.display.set_mode((640, 480), pygame.HWSURFACE|pygame.DOUBLEBUF)
        pygame.display.set_caption('PyPong')
        pygame.mouse.set_visible(False)

        #screen area and table measurements      
        self.rect = self.screen.get_rect()
        self.width = self.rect.width
        self.height = self.rect.height

        self.table = pygame.Rect(30,50,580,420)



    def draw_score (self, player, alignment = 'center'):

        "called by engine to draw the player names and scores"

        if alignment == "left":
            string = " " + str(player.score) + "          " + player.name  
        elif alignment == "right":
            string = player.name + "          " + str(player.score) + " "

        font = pygame.font.Font(None, 32)
        text = font.render(string, True, (128, 128, 128))

        rect = text.get_rect()
        rect.centery = rect.height

        if alignment == 'left':
            rect.left = self.rect.left + 10
        elif alignment == 'right':
            rect.right = self.rect.right - 10
        else:
            rect.centerx = self.rect.centerx

        self.screen.blit(text, rect)



    def draw_line(self, x1, y1, x2, y2):

        """draw a line from (x1, y1) to (x2, y2)"""

        pygame.draw.line(self.screen, (128, 128, 128), (x1, y1), (x2, y2), 2)


    def draw(self):

        "will be called by engine when drawing"
        black = (0,0,0)
        self.screen.fill(black)
    
        #middle line
        self.draw_line(self.width / 2.0-1, 50, self.width / 2.0-1, self.height - 10)
        #top line
        self.draw_line(0, 50, self.width, 50)
        #bottom line   
        self.draw_line(0, self.height - 10, self.width , self.height - 10)







