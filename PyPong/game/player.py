
import pygame
from pygame.locals import *  


class Paddle(pygame.sprite.Sprite):

    """
    A player's paddle. Its image should be a white rectangle
    of width 14, height 74.
    Takes the argument of position, 'left' or 'right', where 
    a left paddle starts at (30,240) and a right paddle at 
    (610,240).    
    
    Also implement the update function as described in its docstring 
    """

  
    def __init__(self, position):

        pygame.sprite.Sprite.__init__(self)

        self.width, self.height = 14,74

        self.image = pygame.Surface([self.width, self.height])
        white = (255,255,255)
        self.image.fill((white))

        self.rect = self.image.get_rect()
        if position == "left":
            self.rect.center = [30,240]
        elif position == "right":
            self.rect.center = [610,240]

        self.top_bound, self.bottom_bound = 50,470

        self.speed = 3



    def move_up(self):

        #move the paddle, select a "speed" of your choice
        self.rect.centery += -self.speed

    def move_down(self):

        #moves the paddle down
        self.rect.centery += self.speed 
    
    def update(self):
        
        """
        needs to correct positions if paddle is moving 
        out of the tables boundaries:
            Top boundary: 50
            Bottom boundary: 470
        """

        if self.rect.top < self.top_bound:
            self.rect.top = self.top_bound
        
        if self.rect.bottom > self.bottom_bound:
            self.rect.bottom = self.bottom_bound

         



class Player(object):

    """Has a name, score count and a paddle"""

    def __init__(self, name, position):

        #initialize player's name, score and paddle here
        #position is either "left" or "right"

        self.score = 0
        self.name = name

        self.paddle = Paddle(position)      






