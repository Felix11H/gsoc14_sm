
import pygame
from pygame.locals import * 

import random, math 


class Ball(pygame.sprite.Sprite):

    """ """

    def __init__(self):

        pygame.sprite.Sprite.__init__(self)

        self.width, self.height = 12,12

        self.image = pygame.Surface([self.width, self.height])
        white = (255,255,255)
        self.image.fill((white))
        
        self.rect = self.image.get_rect()
        self.rect.center = [320,240]

        self.servespeed = 3

        #for velocity in x and y direction.
        self.velx = 0
        self.vely = 0


    def reset(self):

        """Put the ball back in the middle and stop it from moving"""

        self.rect.centerx, self.rect.centery = 320, 240
        self.velx = 0
        self.vely = 0

    
    def update(self):

        """move the ball"""
    
        self.rect.x += self.velx
        self.rect.y += self.vely


    def serve(self):

        angle = 0 

        while abs(angle)<25:
            angle = random.randint(-45, 45)

        # pick a side with a random call
        if random.random() > .5:
            angle += 180

        self.velx = int(self.servespeed * math.cos(math.radians(angle)))
        self.vely = int(self.servespeed * math.sin(math.radians(angle))) 

