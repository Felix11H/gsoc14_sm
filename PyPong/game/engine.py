
import pygame
from pygame.locals import *    

from canvas import Canvas
from player import Player
from ball import Ball



class Engine:


    def __init__(self, p1_name, p2_name):

        """initiates pygame, the canvas, players and ball"""

        pygame.init()
        self.clock = pygame.time.Clock()
        self.running = True

        self.canvas = Canvas()

        self.player1 = Player(p1_name, 'left')
        self.player2 = Player(p2_name, 'right')

        self.ball = Ball()

        #organize the players' paddles in a sprite group "paddles"

        self.paddles = pygame.sprite.Group()
        self.paddles.add(self.player1.paddle)
        self.paddles.add(self.player2.paddle)


        #collect all sprites - ball and paddles - in a sprite group "sprites"

        self.sprites = pygame.sprite.Group()
        self.sprites.add(self.paddles)
        self.sprites.add(self.ball)   
 


    def handle_input(self):

        """
        should handle and react to the following input:

            unique:

            K_ESCAPE: quit the game    
            K_SPACE: if the ball isn't currently moving
                     - it has just been reset -
                     serve the ball (ball.serve() is already implemented.)              


            continuous:

            When K_w (K_s) is pressed, move player1's paddle up (down)
            When K_UP (K_DOWN) is pressed, move player2's paddle up (down)
          
        """

        #controls the window "x"-event
        for event in pygame.event.get():
            if event.type == QUIT:
                self.running = False

            #Keydown events to catch one time button presses
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    self.running = False

                # serve the ball if it isn't moving
                if event.key == K_SPACE:
                    if self.ball.velx == 0 and self.ball.vely == 0:
                        self.ball.serve()


        # continuous input for moving the paddles     
        key = pygame.key.get_pressed()

        if key[pygame.K_w]:
            self.player1.paddle.move_up()
        if key[pygame.K_s]:
            self.player1.paddle.move_down()
        if key[pygame.K_UP]:
            self.player2.paddle.move_up()
        if key[pygame.K_DOWN]:
            self.player2.paddle.move_down()   
       


    def handle_game_events(self):

        '''
        This function should handle the following events:

            -move the ball
            -keep the paddles on the screen
            -bouncing of the ball from paddles
            -bouncing of the ball from sidelines
            -if the ball goes behind a player's baseline:
                score a point for opposite player
                reset the ball  

            HINT: Canvas hast a rect called "table",
                  representing the playing field. 
                  You can use it to check if the ball 
                  is going "out of bounds".
        
        '''

        #moving the ball, keeping the paddles on screen
        self.sprites.update()        

        if pygame.sprite.spritecollideany(self.ball, self.paddles):
            self.ball.velx *= -1
            self.ball.rect.x += self.ball.velx
            
        if self.ball.rect.top < self.canvas.table.top:
            self.ball.vely *= -1
            self.ball.rect.centery += 2*self.ball.vely

        if self.ball.rect.bottom > self.canvas.table.bottom:
            self.ball.vely *= -1
            self.ball.rect.centery += 2*self.ball.vely

        if self.ball.rect.left < self.canvas.table.left:
            self.ball.reset()
            self.player2.score +=1

        if self.ball.rect.right > self.canvas.table.right:
            self.ball.reset()
            self.player1.score +=1
                


    def draw(self):

        self.canvas.draw()

        #uncomment to display scores, onces players are implemented
        self.canvas.draw_score(self.player1, "left")
        self.canvas.draw_score(self.player2, "right")

        '''
        draw the sprites from your sprite group here
        ''' 

        self.sprites.draw(self.canvas.screen)

        pygame.display.flip()

  
    def run(self):

        while self.running:

            # handle input events 
            self.handle_input()
            
            # bouncing of the ball and scoring
            self.handle_game_events()

            self.draw()

            # limits the frames per second (fps)
            self.clock.tick(60)
            pygame.display.set_caption('PyPong  %d fps' % self.clock.get_fps())








