#!/bin/bash         

inputfile=$1 

labelstr=`python comp/figure_label.py` ;

smt run --executable=python --main=comp/plot_data.py $inputfile $labelstr --reason=Test graphic --tag=graphic --label=$labelstr comp/params/plot_data_params_template.py 
