
import sys,os,pickle,uuid
import pylab as pl

import params.plot_data_params_template as params
reload(params)

   
#------------------------------

try:
    label = str(sys.argv[2])
except IndexError:
    label = str(uuid.uuid4())[:13]


input_data_path = sys.argv[1]

#------------------------------


path = input_data_path
pfile = open(path, "rb")
data = pickle.load(pfile)
pfile.close()

xs = range(len(data))
ys = data
pl.plot(xs,ys)

pl.savefig(os.path.join("data/", label + ".png"))



