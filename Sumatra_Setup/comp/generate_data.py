
import sys,os,imp,pickle,time,mytools

import params.generate_data_params_template as params
reload(params)


def main(argv):

    try:
        label = str(sys.argv[1])        
    except IndexError:
        label = time.strftime("%Y%m%d-%H%M%S")     

    #------------------------

    data = mytools.rng_gen(params.low,params.high,params.N)

    #------------------------

    pfile = open("data/%s.p" %label, "wb")
    pickle.dump(data, pfile)
    pfile.close()

    #------------------------


if __name__ == "__main__":
    main(sys.argv)
