
import time

import params.generate_data_params_template as params
reload(params)

label_string = "rnd_%d-%d_N%d_%s" %(params.low, params.high, 
                                   params.N, 
                                   time.strftime("%Y%m%d-%H%M%S"))

if __name__ == "__main__":
    print label_string
