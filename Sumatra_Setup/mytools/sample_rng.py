import numpy as np


def rng_gen(low, high, N=1, seed = None):

    if not seed==None:
        np.random.seed(seed)

    data = np.random.randint(low, high, N)

    return data


